const functions = require('firebase-functions');
const admin = require('firebase-admin');
const Fuse = require('./fuse.min.js');
const _ = require('./lodash.min.js');
const serviceAccount = require("./service-key.json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://savorapp-72shad1d.firebaseio.com"
});

const commonWordsToAvoid = ["product", "bottle", "design", "glass", "liquor", "liqueur", "drink", "beverage", "label"]

const categories = [
    {
        "name": "Mezcal"
    },
    {
        "name": "Canadian Whiskey"
    },
    {
        "name": "White Whiskey/Moonshine"
    },
    {
        "name": "Bourbon"
    },
    {
        "name": "Rum"
    },
    {
        "name": "Vodka"
    },
    {
        "name": "Scotch"
    },
    {
        "name": "Japanese Whisky"
    },
    {
        "name": "Irish Whiskey"
    },
    {
        "name": "Brandy & Cognac"
    },
    {
        "name": "Tequila"
    },
    {
        "name": "Other Imported Whiskey"
    },
    {
        "name": "American Whiskey"
    },
    {
        "name": "Gin"
    },
    {
        "name": "Rye whiskey"
    }
]

let categoryItems = [];
let categoryTicker = 0;
let categorySearchResults = [];
let allKeys = [];
var formatted = []

// let allKeys = ["elk rider bourbon","distilled beverage","liqeur","alcoholic beverage","drink","product",
// "whisky","glass bottle","tennessee whiskey","bottle","tennessee whiskey","Bourbon whiskey","Whiskey","Liquor",
// "Blended whiskey","Rye whiskey","Distillation","Vodka","Scotch whiskey","Heritage Distilling Company"]
// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
const data = require("./savor-db-spirits.json");

// var tickerCount = 0;
// const nestedContent = data;

// function ticker() {
//     tickerCount++;
//     setTimeout(() => {
//         upload();
//     }, 20);
// }

// function upload() {
//     admin.firestore()
//         .collection('spirits')
//         .add(data[tickerCount])
//         .then((res) => {
//             console.log("Document ", tickerCount);
//             ticker();
//         })
//         .catch((error) => {
//             console.error("Error writing document: ", error);
//         });
// }

// upload();

// data && Object.keys(nestedContent).forEach(key => {
//     const nestedContent = data[key];
//     if (typeof nestedContent === "object") {
//         Object.keys(nestedContent).forEach(docTitle => {
//             admin.firestore()
//                 .collection(key)
//                 .add(nestedContent[docTitle])
//                 .then((res) => {
//                     console.log("Document ", ticker);
//                     ticker++;
//                 })
//                 .catch((error) => {
//                     console.error("Error writing document: ", error);
//                 });
//         });
//     }
// });

exports.findBestMatch = functions.https.onRequest((request, response) => {
    console.log(request.body, "request")
    // function onInit(){
    // get all data from app
    let webSearchKeys = request.body.webLabels;
    let bestGuessKey = request.body.bestGuess;
    let labelSearchKeys = request.body.labels;
    let textSearchKeys = request.body.fullText;
    let logoKeys = request.body.logoLabels;
    console.log("webSearchKeys Array", webSearchKeys, "bestGuessKey string", bestGuessKey, "labelSearchKeys Array", labelSearchKeys, "textSearchKey String", textSearchKeys, "logoKeys Empty Array", logoKeys);
    // categoryItems = [];
    // categoryTicker = 0;
    // categorySearchResults = [];
    // let allKeys = [];
    // combine all searchable keys together
    allKeys = webSearchKeys.concat(labelSearchKeys).concat(logoKeys);
    allKeys.push(bestGuessKey);
    allKeys.push(textSearchKeys);

    // let categories = [];
    console.log("allkeys value is", allKeys);
    console.log("categories", categories);

    // filter common words here
    filterCommonWords(allKeys)
    // decide category from the Vision results

    decideCategory(categories, formatted)

    response.send("ok")

    // get all item names of that category (other fields in future)


    // rank the result as per maximum occurence. If no max occurrence, as per rank ?? 
});

function decideCategory(categories, formatted) {
    console.log("Data at decideCategory", formatted);
    console.log("categories decideCategory", categories);
    var options = {
        keys: ['name'],
        minMatchCharLength: 3,
        tokenize: true,
        includeScore: true
    }
    var fuse = new Fuse(categories, options)
    console.log("fuser at decideCategory", fuse)
    formatted.forEach(function (key) {
        console.log("key", key)
        let matchData = fuse.search(key);
        console.log("matchDataCategory", matchData)
        matchData.forEach(function (matchResult) {
            if (matchResult.score > 0.55 && categorySearchResults.indexOf(key) === -1) {
                console.log("(matchData", matchData)
                categorySearchResults.push(key)
            }
        })
    })
    categorySearchResults = _.uniq(categorySearchResults)
    console.log("categorySearchResults", categorySearchResults);
    fetchNextCategory();
}

fetchCategoryItems = async (category) => {
    console.log(category)
    const r = await admin.firestore()
        .collection('item')
        .where("category", "==", category)
        .get();
    if (r.docs) {
        const docs = r.docs;
        for (const doc of docs) {
            categoryItems.push({ name: doc.data().name, id: doc.id });
        }
    }
    fetchNextCategory();
}


function fetchNextCategory() {
    console.log("categorySearchResults fetchNextCategory", categorySearchResults);
    console.log("categoryTicker", categoryTicker);
    if (categoryTicker <= categorySearchResults.length - 1) {
        fetchCategoryItems(categorySearchResults[categoryTicker]);
        categoryTicker++;
    }
    else {
        console.log("categoryItems");
        console.log(categoryItems);
        findFinalMatch()
    }
}

function findFinalMatch() {
    let finalSearchResults = []
    var options = {
        keys: ['name'],
        minMatchCharLength: 4,
        tokenize: true,
        includeScore: true
    }
    var fuser = new Fuse(categoryItems, options)

    allKeys.forEach(key => {
        console.log(key);
        let match = fuser.search(key);
        console.log(match)
        if (match && match.length && match[0]) {
            let sortedMatches = _.sortBy(match, [function (o) { return o.score }]);
            finalSearchResults.push(sortedMatches[0])
        }
    })
    console.log(finalSearchResults);
}

// filter common words from the vision results so we spend less time in search and can avoid unwanted results
function filterCommonWords(a) {
    console.log("a", a)
    var options = {
        keys: ['name'],
        minMatchCharLength: 3,
        tokenize: true,
        includeScore: true
    }
    var removeData = [
        {
            "name": "product"
        },
        {
            "name": "bottle"
        },
        {
            "name": "design"
        },
        {
            "name": "glass"
        },
        {
            "name": "liquor"
        },
        {
            "name": "liqueur"
        },
        {
            "name": "drink"
        },
        {
            "name": "beverage"
        },
        {
            "name": "label"
        },
        {
            "name": "Bottle"
        },
        {
            "name": "Product"
        }
    ]
    var fuse = new Fuse(removeData, options)
    console.log("fuser Value is", fuse)
    allKeys.forEach(function (key) {
        console.log("key data is", key)
        var match = fuse.search(key)
        console.log("match result is", match)
        match.forEach(function (searchResult) {
            if (searchResult.score > 0.33 && formatted.indexOf(key) === -1)
                formatted.push(key)
            console.log("formattedData", formatted);
        })
    });
}

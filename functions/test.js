const Fuse = require('./fuse.min.js');
const _ = require('./lodash.min.js');

const commonWordsToAvoid = ["product", "bottle", "design", "glass", "liquor", "liqueur", "drink", "beverage", "label"]
const items = ["Label", "Design", "Whiskey", "The best new U.S. & international label designs", "Liqueur", "Bourbon whiskey", "Rye whiskey", "Product", "Miniature", "Bottle",
"distilled beverage", "liqueur", "alcoholic beverage", "product", "drink", "whisky", "glass bottle",
"Great Lakes Distillery Whiskey Kinnickinnic", "Westchester Golf Course",
"HERITAGE DISTILLING DUAL BARREL COLLECTION WHISKEY DUAL AGED USING USED CASKS Proof",
"design"]


function filterCommonWords(){
    var options = {
        keys: ['name'],
        minMatchCharLength: 3,
        tokenize: true,
        includeScore: true
    }
    
    var string = "bottle"
    // allKeys.forEach(key => {
    //     let match = fuser.search(key);
    //     if (match && match.length && match[0]) {
    //         categorySearchResults.push(match[0].name)
    //     }
    // })

    var formatted = []
    items.forEach(word=>{
        formatted.push({name: word})
    })
    var fuser = new Fuse(formatted, options)
    console.log(fuser.search(string))
}


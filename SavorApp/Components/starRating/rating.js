import React, {Component} from 'react'
import {View} from 'react-native'
import StarRating from 'react-native-star-rating';
class RatingComponent extends Component{
    constructor(props) {
        super(props);
        this.state = {
          starCount: 2.5
        };
      }
    
      onStarRatingPress(rating) {
        this.setState({
          starCount: rating
        });
      }
    
          render() {
       return (
        <StarRating
        disabled={false}
        emptyStar={'ios-star-outline'}
        fullStar={'ios-star'}
        halfStar={'ios-star-half'}
        iconSet={'Ionicons'}
        maxStars={7}
        rating={this.props.starCount}
        selectedStar={(rating,star) => this.props.textChange({ Name: rating, class:'Star'})}
        fullStarColor={'red'}
      />
            )
}
}

export default RatingComponent;
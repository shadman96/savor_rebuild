import React, { Component } from "react";
import { Text, TouchableOpacity, View, StyleSheet } from "react-native";
import Modal from "react-native-modal";
import { AppStyles, AppSizes } from '@theme/';
import { Icon } from 'native-base';
export default class GreetModal extends Component {
  state = {
    isModalVisible: true
  };

  _toggleModal = () =>
    this.setState({ isModalVisible: !this.state.isModalVisible });

  camera = () => {
    console.log("camera")
    this.props.navigation.navigate('CameraComponent')
  }
  render() {
    return (
      <View style={styles.container}>
        <Modal isVisible={this.props.isModalVisible}>
          <View style={styles.Messagecontainer}>
            <Text style={styles.Thank}>Thank you!</Text>
            <Text style={styles.ThankMessage}>Thank you for submitting  information  about  Hibiki12. Your  contribution  will  help  us  build  our database  and  allow  us  to  develop new  and more accurate features.</Text>
            <View>
              <TouchableOpacity onPress={() => this.camera()} style={styles.buttonDiv}>
                <Icon type="FontAwesome" name='camera' style={styles.Save} />
                <Text style={styles.TakePicture}>Take another Picture</Text>
              </TouchableOpacity>
            </View>
            <TouchableOpacity onPress={() => this.props.toggleModal()}>
              <Text>Hide me!</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.props.toggleModal()}>
              <Text>ok</Text>
            </TouchableOpacity>
          </View>
        </Modal>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  Messagecontainer: {
    width: AppSizes.screen.width * 0.9,
    height: AppSizes.screen.width * 0.9,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingHorizontal: 20,
    borderRadius: 3


  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
  },
  Thank: {
    fontSize: 25,
    color: 'black',
    fontWeight: 'bold',
    paddingVertical: 16,

  },
  ThankMessage: {
    fontSize: 15,
    color: '#8A8A8A',
    fontWeight: 'bold'
  },
  TakePicture: {
    fontSize: 15,
    color: '#76C7BF',
    fontWeight: 'bold'
  },
  Save: {
    right: 20,
    backgroundColor: '#006390',
    padding: 20,
    borderRadius: 90,
    color: '#FFF',
    fontWeight: '600',
    fontSize: 42,
  },
  buttonDiv: {
    display: 'flex',
    marginTop: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 16,
    alignItems: 'center',
    paddingVertical: 16,
  }
})

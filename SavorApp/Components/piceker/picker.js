import React, { Component } from 'react'
import { View, TextInput, StyleSheet, Text } from 'react-native';
import { Container, Header, Left, Picker, Body, Right, Button, Icon, Title, Content, H2, List, ListItem, Form, Item, Input, Label } from 'native-base';

class PickerComponent extends Component {
  constructor(props) {
    state = {
      value: 0.2
    };
    super(props);
    this.state = {
      PickerCategory: 'key1',
      PickerType: 'key3'

    };
  }
  onValueChange({ value: itemValue }) {
    console.log(itemValue,"itemValue")
    this.setState({
      PickerCategory: itemValue,
      PickerType: itemValue
    });
  }
  render() {
    return (
      <View style={styles.GeneralPicker}>
        <Label style={styles.TextBlack}>Category</Label>
        <Item picker>
          <Picker
            mode="dropdown"
            iosIcon={<Icon name="ios-arrow-down-outline" />}
            style={{ width: undefined }}
            placeholder="Select your SIM"
            placeholderStyle={{ color: "#bfc6ea" }}
            placeholderIconColor="#007aff"
            selectedValue={this.props.PickerCategory}
            onValueChange={(itemValue, itemIndex,c) => this.props.textChange({ Name: itemValue,id:itemIndex, class:'category'})}>
            <Picker.Item label="Whiskey" value="Whiskey" />
            <Picker.Item label="Cigar" value="Cigar" />
            <Picker.Item label="Wine" value="Wine" />
            <Picker.Item label="Whiskey" value="Whiskey" />
            <Picker.Item label="Food" value="Food" />
            <Picker.Item label="Other" value="Other" />
          </Picker>
        </Item>

        <Label style={styles.TextBlack}>Distillery/</Label>
        <TextInput
          style={{ paddingVertical: 16 }}
          placeholder="Hibiki"
          onChangeText={(itemValue,type) => this.props.textChange({ Name: itemValue, class:'Distillery'})}
          value ={this.props.Distillery}
        />
        <Label style={styles.TextBlack}>Type</Label>

        <Item picker>
          <Picker
            mode="dropdown"
            iosIcon={<Icon name="ios-arrow-down-outline" />}
            style={{ width: undefined }}
            placeholder="Select your SIM"
            placeholderStyle={{ color: "#bfc6ea" }}
            placeholderIconColor="#007aff"
            selectedValue={this.props.PickerType}
            onValueChange={(itemValue, itemIndex) => this.props.textChange({ Name: itemValue , id:itemIndex, class:'type' })}
          >
            <Picker.Item label="Blended" value="Blended" />
            <Picker.Item label="Blended Malt" value="Blended Malt" />
            <Picker.Item label="Bonded" value="Bonded" />
            <Picker.Item label="Single Barrel" value="Single Barrel" />
            <Picker.Item label="Straight" value="Straight" />
          </Picker>
        </Item>
        <Label style={styles.TextBlack}>Vintage</Label>
        <TextInput
          style={{ paddingVertical: 16 }}
          placeholder="None"
          onChangeText={(itemValue,type) => this.props.textChange({ Name: itemValue, class:'Vintage'})}
          value ={this.props.Vintage}
        />
        <Label style={styles.TextBlack}>Age</Label>
        <TextInput
          style={{ paddingVertical: 16 }}
          placeholder="12"
          onChangeText={(itemValue,type) => this.props.textChange({ Name: itemValue, class:'Age'})}
          value ={this.props.Age}
        />
      </View>
    )
  }


}
const styles = StyleSheet.create({
  GeneralPicker: {
    width: 300,
   },
   TextBlack:{color:'black'},

});
export default PickerComponent;
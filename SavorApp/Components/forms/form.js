import React, { Component } from 'react';
import { Item, Input, Label } from 'native-base';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
} from 'react-native';

const FormComponent = (props) => {
    const { Email, password, User, Age,secureTextEntry } = props
    return (
        <View style={styles.container}>
            {Email &&
                <Item stackedLabel>
                    <Label style={styles.LabelForm}>{Email}</Label>
                    <Input style={styles.FormColor} value={props.email} onChangeText={(text,type) => props.inputChangeText(text,"email")} />
                </Item>}
            {password &&
                <Item stackedLabel last>
                    <Label style={styles.LabelForm}>{password}</Label>
                    <Input style={styles.FormColor} value={props.pass} onChangeText={(text,type) => props.inputChangeText(text,"password")} secureTextEntry={secureTextEntry}/>
                </Item>
            }
            {User &&
                <Item stackedLabel last>
                    <Label style={styles.LabelForm}>{User}</Label>
                    <Input style={styles.FormColor} value={props.username} onChangeText={(text,type) => props.inputChangeText(text,"username")}/>
                </Item>
            }
            {Age &&
                <Item stackedLabel last>
                    <Label style={styles.LabelForm}>{Age}</Label>
                    <Input style={styles.FormColor} value={props.age} onChangeText={(text,type) => props.inputChangeText(text,"age")}  />
                </Item>
            }

            

        </View>
    );
}
const styles = StyleSheet.create({
    LabelForm: {
        color: 'white',
        fontWeight:'500',
        fontSize: 14
    },
    container: {
        marginTop: '10%'
    },
    FormColor: {
        color: 'white',
        fontWeight:'100',
        fontSize: 13,
        paddingLeft: 0
    },
    item:{
        marginBottom: 20
    }
})
export default FormComponent;

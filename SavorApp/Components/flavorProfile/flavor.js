import React, { Component } from 'react'
import { View, StyleSheet, Dimensions, Text, Slider } from 'react-native'
import { Container, Header, Left, Picker, Body, Right, Button, Icon, Title, Content, H2, List, ListItem, Form, Item, Input, Label } from 'native-base';
const { width, height } = Dimensions.get('window')

const flavorProfile = [
  'Smokey',
  'Peaty',
  'Spicy',
  'Herbal',
  'Oily',
  'Full',
  'Rich',
  'Sweet',
  'Briny',
  'Salty',
  'Vanilla',
  'Tart',
  'Fruity',
  'Floral'
];

class FlavorProfile extends Component {
  constructor(props) {
    state = {
      value: new Array(flavorProfile.length).fill(0)
    };
    super(props);
    this.state = {
     value: new Array(flavorProfile.length).fill(0)
    };
  }
  render() {
    console.log("flavorProfile",this.props)
    return (
      <View>
        {this.props.flavorProfile.map((flavor, index) => {
          return (
            <View style={styles.flavorProfile}>
              <View style={styles.flavorTitle}>
                <Text style={{ fontSize: 20, FontWeight: 'bold' }}>{flavor}</Text>
              </View>
              <View style={styles.slider}>
                <Slider style={{ width: 150 }} value={this.props.flavor[index]}
                  onValueChange={update => {
                    this.props.flavorValueChange(update, index)
                  }} minimumValue={0} maximumValue={10} step={1}
                  handleBorderColor="red" handleBorderWidth={3}
                />
                <Text>
                  {this.props.flavor[index]}
                </Text>
              </View>
            </View>
          )
        }
        )}
      </View>
    )
  }


}
const styles = StyleSheet.create({
  flavorProfile: {
    width: width - 40,
    flexDirection: 'row',
    justifyContent: "space-between",
    paddingVertical: 16,
    paddingHorizontal: 16,
    // paddingBottom:16
    borderBottomWidth:1,
    borderBottomColor:'#EFEFEF'
  },
   
  slider: {
    marginHorizontal: 10,
    justifyContent: 'center'
  }
});
export default FlavorProfile;
import React, { Component } from 'react'
import { View, ActivityIndicator, StyleSheet } from 'react-native'

const SpinnerComponent = (props) => {
    // const { size } = (props)
    return (
        <View style={styles.SpinnerStyle}>
            <ActivityIndicator size={ props.size || 'large'} />
        </View>
    )
};
const styles = StyleSheet.create({
    SpinnerStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})
export default SpinnerComponent;
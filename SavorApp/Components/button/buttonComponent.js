import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
} from 'react-native';
import { Container, Header, Content, Button } from 'native-base';

const ButtonComponent = (props) => {
    const { onPress } = props;
    return (
        <View style={styles.container}>
            {props.link === 'LOGIN' &&
                <TouchableOpacity style={styles.button} onPress={onPress}>
                    <Text style={styles.text}> {props.link} </Text>
                </TouchableOpacity>
            }
            {props.block &&
                <Button full onPress={onPress} style={styles.button}>
                    <Text style={styles.text}>{props.link}</Text>
                </Button>
            }
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
    },
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FFD43F',
        borderRadius: 10,
        height: 44,
        paddingHorizontal: 15
    },
    text: {
        color: '#444',
        fontSize: 13,
        fontWeight: 'normal'
    },
    countContainer: {
        alignItems: 'center',
        padding: 10
    },
    countText: {
        color: '#FF00FF'
    }
})
export default ButtonComponent;
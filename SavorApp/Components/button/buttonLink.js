import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
} from 'react-native';

    const ButtonLinkComponent = (props) =>{
    const  { onPress } = props;
        return (
            <View style={styles.container}>
                <TouchableOpacity style={styles.button} onPress={onPress}>
                    <Text style={styles.text}> {props.link} </Text>
                </TouchableOpacity>
            </View>
        );
    }

const styles = StyleSheet.create({
    container: {
      justifyContent: 'center',
    },
    button: {
      alignItems: 'center',
      justifyContent: 'center', 
      borderRadius: 5,
      height:44,
      marginBottom:'2%',
     },
    text:{
        color:'rgb(255,255,238)',
        fontSize:14,
        fontWeight:'normal'
    },
    countContainer: {
      alignItems: 'center',
      padding: 10
    },
    countText: {
      color: '#FF00FF'
    }
  })
  export default ButtonLinkComponent;
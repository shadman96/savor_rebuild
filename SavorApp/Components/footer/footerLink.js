import React, { Component } from 'react';
import { Container, Header, Content, Footer   } from 'native-base';
import {DrawerItems} from 'react-navigation';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
} from 'react-native';
 const FooterLinkComponent = (props) => {
    const { onPress } = props;
    return (
        <Container>
        // <Content>
        // <DrawerItems {...props}/>
        // </Content>
            <Footer style={{backgroundColor: 'transparent'}}>
                <View>
                {props.link &&
                
                    <TouchableOpacity style={styles.button} onPress={onPress}>
                        <Text style={styles.text}> {props.link} </Text>
                    </TouchableOpacity>
                }
                {!props.link &&
                    <TouchableOpacity style={styles.button} onPress={onPress}>
                    <Text style={styles.text}> Login </Text>
                   </TouchableOpacity>
                }
                </View>  
             </Footer>  
        </Container>
    );
}
const styles = StyleSheet.create({
    container: {
      justifyContent: 'center',
      paddingHorizontal: 20,
      marginTop:10
    },
    button: {
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: 'orange',
      paddingLeft: 100,
      paddingRight: 100,
      borderRadius: 5,
      height:60,
      marginBottom:'5%'
    },
    text:{
        color:'#0379FF',
        fontSize:16
    },
    countContainer: {
      alignItems: 'center',
      padding: 10
    },
    countText: {
      color: '#FF00FF'
    }
  })

export default FooterLinkComponent;
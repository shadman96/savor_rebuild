import React, { Component } from 'react';
import { Container, Header, Content, Footer   } from 'native-base';
import {DrawerItems} from 'react-navigation';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Dimensions
} from 'react-native';
import firebase from 'react-native-firebase'
const { width, height } = Dimensions.get('window')

 const FooterComponent = (props) => {
     
    signOutUser = async () => {
        try {
            await firebase.auth().signOut();
        } catch (e) {
            console.log(e);
        }
    }
    
    return (
        <Container>
        <Content>
        <DrawerItems {...props}/>
        </Content>
            <Footer style={{backgroundColor: 'transparent', bottom:15}}>
                <View style={styles.container}>
                    <TouchableOpacity style={styles.button} onPress={() => this.signOutUser()}>
                    <Text style={styles.text}> LogOut </Text>
                   </TouchableOpacity>
                </View>  
             </Footer>  
        </Container>
    );
}
const styles = StyleSheet.create({
    container: {
      justifyContent: 'center',
      paddingHorizontal: 20,
      marginTop:10,
      alignItems:'center',

    },
    button: {
      backgroundColor: 'orange',
      paddingLeft: 100,
      paddingRight: 100,
      borderRadius: 5,
      height:60,
      marginBottom:'5%',
      justifyContent:'center',
        alignItems:'center',
     },
    text:{
        color:'#0379FF',
        fontSize:16,
        width: 0.17* width
    },
    countContainer: {
        justifyContent:'center',
        alignItems:'center',
        // padding: 10
    },
    countText: {
      color: '#FF00FF'
    }
  })

export default FooterComponent;
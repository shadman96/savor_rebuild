import React, { Component } from 'react'
import { View, TextInput, StyleSheet, Text } from 'react-native';
import { Container, Header, Left, Picker, Body, Right, Button, Icon, Title, Content, H2, List, ListItem, Form, Item, Input, Label } from 'native-base';

class PickerComponent extends Component {
  constructor(props) {
    state = {
      value: 0.2
    };
    super(props);
    this.state = {
      PickerCategory: 'key1',
      PickerType: 'key3'

    };
  }
  onValueChange({ value: itemValue }) {
    this.setState({
      PickerCategory: itemValue,
      PickerType: itemValue
    });
  }
  render() {
    return (
      <View style={styles.GeneralPicker}>
      <Item stackedLabel>
              <Label>Category</Label>
              <Item picker>
              <Picker
                mode="dropdown"
                iosIcon={<Icon name="ios-arrow-down-outline" />}
                style={{ width: undefined }}
                placeholder="Select your SIM"
                placeholderStyle={{ color: "#bfc6ea" }}
                placeholderIconColor="#007aff"
                selectedValue={this.state.PickerCategory}
                onValueChange={(itemValue, itemIndex) => this.setState({ PickerCategory: itemValue })}>
                <Picker.Item label="Whiskey" value="key0" />
                <Picker.Item label="Cigar" value="key1" />
                <Picker.Item label="Wine" value="key2" />
                <Picker.Item label="Whiskey" value="key3" />
                <Picker.Item label="Food" value="key4" />
                <Picker.Item label="Other" value="key5" />
              </Picker>
            </Item>
            </Item>
            <Item stackedLabel>
            <Label>Username</Label>
            <Input placeholder="Type here to translate!"/>
          </Item>
        <Label>Distillery/</Label>
        <TextInput
          style={{ paddingVertical: 16 }}
          placeholder="Type here to translate!"
          onChangeText={(text) => this.setState({ text })}
        />
        <Label>Type</Label>

        <Item picker>
          <Picker
            mode="dropdown"
            iosIcon={<Icon name="ios-arrow-down-outline" />}
            style={{ width: undefined }}
            placeholder="Select your SIM"
            placeholderStyle={{ color: "#bfc6ea" }}
            placeholderIconColor="#007aff"
            selectedValue={this.state.PickerType}
            onValueChange={(itemValue, itemIndex) => this.setState({ PickerType: itemValue })}
          >
            <Picker.Item label="Blended" value="key0" />
            <Picker.Item label="Blended Malt" value="key1" />
            <Picker.Item label="Bonded" value="key2" />
            <Picker.Item label="Single Barrel" value="key3" />
            <Picker.Item label="Straight" value="key4" />
          </Picker>
        </Item>
        <Label>Vintage</Label>
        <TextInput
          style={{ paddingVertical: 16 }}
          placeholder="Type here to translate!"
          onChangeText={(text) => this.setState({ text })}
        />
        <Label>Age</Label>
        <TextInput
          style={{ paddingVertical: 16 }}
          placeholder="Type here to translate!"
          onChangeText={(text) => this.setState({ text })}
        />
      </View>
    )
  }


}
const styles = StyleSheet.create({
  GeneralPicker: {
    width: 300,
   },
});
export default PickerComponent;






import React, { Component } from 'react'
import { View, StyleSheet, Dimensions, Text, Slider } from 'react-native'
import { Container, Header, Left, Picker, Body, Right, Button, Icon, Title, Content, H2, List, ListItem, Form, Item, Input, Label } from 'native-base';
const { width, height } = Dimensions.get('window')



class FlavorProfile extends Component {
  constructor(props) {
    state = {
      flavorProfile :[
        {
          name: 'Smokey',
          key: 1
        },
        {
          name: 'Peaty',
          key: 2
        },
        {
          name: 'Spicy',
          key: 3
        },
        {
          name: 'Herbal',
          key: 4
        },
        {
          name: 'Oily',
          key: 5
        },
        {
          name: 'Full',
          key: 6
        },
        {
          name: 'Rich',
          key: 7
        },
        { name: 'Sweet', key: 8 },
        {
          name: 'Briny',
          key: 9
        },
        {
          name: 'Salty',
          key: 10
        },
        {
          name: 'Vanilla',
          key: 11
        },
        {
          name: 'Tart',
          key: 12
        },
        {
          name: 'Fruity',
          key: 13
        },
        {
          name: 'Floral',
          key: 14
        }
      ]
    };
    super(props);
    this.state = {


    };
  }
  render() {
    return (
      <View>
        {this.state.flavorProfile.map((flavor, index) => {
          return (
            <View style={styles.flavorProfile}>
              <View style={styles.flavorTitle}>
                <Text style={{ fontSize: 20, FontWeight: 'bold' }}>{flavor.name}</Text>
              </View>
              <View style={styles.slider}>
                <Slider style={{ width: 150 }} value={flavor.key}
                  onValueChange={value => this.setState({ value })} minimumValue={0} maximumValue={10} step={1}
                  handleBorderColor="red" handleBorderWidth={3}
                />
                <Text>
                  {flavor.key}
                </Text>
              </View>
            </View>
          )
        }
        )}
      </View>
    )
  }


}
const styles = StyleSheet.create({
  flavorProfile: {
    width: width - 40,
    flexDirection: 'row',
    justifyContent: "space-between",
    paddingVertical: 16,
    paddingHorizontal: 16,
    // paddingBottom:16
  },
  flavorTitle: {

  },
  slider: {
    marginHorizontal: 10,
    justifyContent: 'center'
  }
});
export default FlavorProfile;




takePicture = async function() {
  if (this.camera) {
    const options = { quality: 0.5, base64: true };
    const data = await this.camera.takePictureAsync(options)
    console.log(data.uri);
  }
};


















import React, { Component } from 'react'
import {
  Platform,
  AppRegistry,
  Dimensions,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from 'react-native';
import { RNCamera } from 'react-native-camera';



class CameraComponent extends Component {
  static navigationOptions = {
    header: null
  }
  state = {
    loading: false,
  };


  takePicture() {
    if (!this.state.loading) {
      this.setState({
        loading: true
      });

      const options = { quality: 0.5, base64: true };
      this.camera.capture({ metadata: options })
        .then((data) => {

          resizeImage(data.path, (resizedImageUri) => {
            NativeModules.RNImageToBase64.getBase64String(resizedImageUri, async (err, base64) => {
              // Do something with the base64 string
              if (err) {
                console.error(err)
              }
              console.log('converted to base64');
              // ToastAndroid.show('converted to base64', ToastAndroid.SHORT);

              let result = await checkForLabels(base64);
              console.log(result);
              // ToastAndroid.show(JSON.stringify(result), ToastAndroid.SHORT);

              //custom filter
              let filteredResult = filterLabelsList(result.responses[0], 0.3);
              displayResult(filteredResult);

              this.setState({
                loading: false
              });
            })
          })
        })
        .catch(err => console.error(err));
    } else {
      console.log('NO GO' + this.state.loading)
    }
  }

  checkForLabels = async (base64) => {

    return await
      fetch(`https://vision.googleapis.com/v1/images:annotate?key=${AIzaSyDEwjX3dVRFDW2USdUtcjWlkOwgvjp8wl0}`, {
        method: 'POST',
        body: JSON.stringify({
          "requests": [
            {
              "image": {
                "content": base64
              },
              "features": [
                {
                  "type": "LABEL_DETECTION"
                }
              ]
            }
          ]
        })
      }).then((response) => {
        return response.json();
      }, (err) => {
        console.error('promise rejected')
        console.error(err)
      });
  }
  render() {
    return (
      <View style={styles.container}>
        <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          style={styles.preview}
          type={RNCamera.Constants.Type.back}
          flashMode={RNCamera.Constants.FlashMode.on}
          permissionDialogTitle={'Permission to use camera'}
          permissionDialogMessage={'We need your permission to use your camera phone'}
          onGoogleVisionBarcodesDetected={({ barcodes }) => {
            console.log(barcodes)
          }}
        />
        <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center', }}>
          <TouchableOpacity
            onPress={this.takePicture.bind(this)}
            style={styles.capture}
          >
            <Text style={{ fontSize: 14 }}> SNAP </Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black'
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20
  }
});


export default CameraComponent;





































takePicture() {
  if (!this.state.loading) {
      this.setState({
          loading: true
      });

      const options = {};
      this.RNCamera.capture({metadata: options})
          .then((data) => {
              resizeImage(data.path, (resizedImageUri) => {
                  NativeModules.RNImageToBase64.getBase64String(resizedImageUri, async (err, base64) => {
                      // Do something with the base64 string
                      if (err) {
                          console.error(err)
                      }
                      console.log('converted to base64');
                      // ToastAndroid.show('converted to base64', ToastAndroid.SHORT);

                      let result = await checkForLabels(base64);
                      console.log(result);
                      // ToastAndroid.show(JSON.stringify(result), ToastAndroid.SHORT);

                      //custom filter
                      let filteredResult = filterLabelsList(result.responses[0], 0.3);
                      displayResult(filteredResult);

                      this.setState({
                          loading: false
                      });
                  })
              })
          })
          .catch(err => console.error(err));
  } else {
      console.log('NO GO' + this.state.loading)
  }
}
}

function displayResult(filteredResult) {
let labelString = '';
let count = 1;
if (filteredResult.length > 1) {
  labelString = '... or it might be ';
  filteredResult.forEach((resLabel) => {
      if (count == filteredResult.length) {
          labelString += 'a ' + resLabel.description + '! I\'m pretty sure! Maybe.'
      } else if (count == 1) {

      } else {
          labelString += 'a ' + resLabel.description + ' or '
      }
      count++;
  });

  Alert.alert(
      'Its a ' + filteredResult[0].description + '!',
      labelString
  );
} else {
  Alert.alert(
      'Its a ' + filteredResult[0].description + '!'
  );
}
}

// according to https://cloud.google.com/vision/docs/supported-files, recommended image size for labels detection is 640x480
function resizeImage(path, callback, width = 640, height = 480) {
ImageResizer.createResizedImage(path, width, height, 'JPEG', 80).then((resizedImageUri) => {
  callback(resizedImageUri);

}).catch((err) => {
  console.error(err)
});
}

//run filter for frontend side logic (filter for hotdog, if you wanna do a "is hotdog or not" app)
function filterLabelsList(response, minConfidence = 0) {
let resultArr = [];
response.labelAnnotations.forEach((label) => {
  if (label.score > minConfidence) {
      resultArr.push(label);
  }
});
return resultArr;
}

// API call to google cloud
async function checkForLabels(base64) {

return await
  fetch(config.googleCloud.api + config.googleCloud.apiKey, {
      method: 'POST',
      body: JSON.stringify({
          "requests": [
              {
                  "image": {
                      "content": base64
                  },
                  "features": [
                      {
                          "type": "LABEL_DETECTION"
                      }
                  ]
              }
          ]
      })
  }).then((response) => {
      return response.json();
  }, (err) => {
      console.error('promise rejected')
      console.error(err)
  });
}





import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Dimensions,
    TouchableHighlight,
    Image,
    Text,
} from 'react-native';
import { RNCamera as Camera } from 'react-native-camera';
import { AsyncStorage } from 'react-native';
import CaptureButton from './button'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#000',
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width
    },
    capture: {
        width: 70,
        height: 70,
        borderRadius: 35,
        borderWidth: 5,
        borderColor: '#FFF',
        marginBottom: 15,
    },
    cancel: {
        position: 'absolute',
        right: 20,
        top: 20,
        backgroundColor: 'transparent',
        color: '#FFF',
        fontWeight: '600',
        fontSize: 17,
    },
    Save: {
        position: 'absolute',
        left: 20,
        top: 20,
        backgroundColor: 'transparent',
        color: '#FFF',
        fontWeight: '600',
        fontSize: 17,
    }
});

class TakePictureScreen extends Component {

    camerapic = [];
    constructor(props) {
        super(props);
        this.state = {
            path: null,
            loading: false,
        };
    }


    componentWillMount() {
        this.retrieveData()
    }

    takePicture = async (err) => {
        console.log("data")
        if (!this.state.loading) {
            this.setState({
                loading: true
            });
            console.log(err)
            try {
                const data = await this.camera.takePictureAsync();
                this.setState({ path: data.uri });
                // this.props.updateImage(data.uri);
                // console.log('Path to image: ' + data.uri);
            }
            catch (err) {
                console.log('err: ', err);
            }
        };
        this.setState({
            loading: false
        });
    }

    /*-----Cancel------*/

    cancel = () => {
        console.log('cancel')
        // alert('cancel')
        console.log(this.state.path, "path State")
        this.setState({
            path:null
        })


    }
    /*----Save----*/
    save = () => {
        console.log('save')
        // alert('save')
        console.log(this.state.path, "path State")
        let data = { cameraurl: this.state.path }
        this.camerapic.push({cameraurl: this.state.path})
        AsyncStorage.setItem('CameraPics', JSON.stringify(this.camerapic))
            .catch((err) => {
                console.log('err', err)
            })

            this.setState({
                path:null
            })
    }

    retrieveData = async () => {
        try {
            value = await AsyncStorage.getItem('CameraPics');
            //  value = await AsyncStorage.clear('Activity') 

            if (value !== null) {
                console.log("CameraPics", value);

                console.log("typeOf", typeof (JSON.parse(value)))
                this.camerapic = JSON.parse(value)
            }
        } catch (error) {
            Toast.show({
                text: error,
                duration: 3000,
                position: "top"
            })
            console.log(error)
        }
    }
    renderCamera() {
        return (
            <Camera
                ref={(cam) => {
                    this.camera = cam;
                }}
                style={styles.preview}
                flashMode={Camera.Constants.FlashMode.on}
                permissionDialogTitle={'Permission to use camera'}
                permissionDialogMessage={'We need your permission to use your camera phone'}
                onGoogleVisionBarcodesDetected={({ barcodes }) => {
                    console.log(barcodes,"barcodes")
                }}
            >
                <View><Text>{this.state.loading}</Text></View>

                <TouchableHighlight
                    style={styles.capture}
                    onPress={this.takePicture.bind(this)}
                    underlayColor="rgba(255, 255, 255, 0.5)"
                >
                    <View />
                </TouchableHighlight>
            </Camera>
        );
    }

    renderImage() {
        return (
            <View>
                <Image
                    source={{ uri: this.state.path }}
                    style={styles.preview} />
                <Text
                    style={styles.cancel}
                    onPress={this.cancel.bind(this)}>Cancel
        </Text>
                <Text
                    style={styles.Save}
                    onPress={this.save.bind(this)}> Save
        </Text>
            </View>
        );
    }

    render() {
        console.log('Starting app');

        return (
            <View style={styles.container}>
                {this.state.path ? this.renderImage() : this.renderCamera()}
            </View>
        );
    }
}
export default TakePictureScreen;
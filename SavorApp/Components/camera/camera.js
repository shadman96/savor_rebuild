import React, { Component } from 'react';
import {
    View,
    Image,
    Text
} from 'react-native';
import { RNCamera as Camera } from 'react-native-camera';
import SpinnerComponent from '../spinner/spinner';
import { Icon } from 'native-base';
import config from '../../config';
import styles from '../../Pages/imagePreview/previewstyle';
import sampleImage from '../../rye';

export default class CameraComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            path: null,
            loading: false,
        };
    }


    // componentDidMount() {
    //     // const data= 
    //     //  this.setState({
    //     //     path: data.uri,
    //     //     base64: data.base64
    //     // })
    //     // console.log(data)
    //     // console.log('previewcontainer');
    //     // console.log(sampleImage)
    //     this.checkForLabels();
    // }


    takePicture = async () => {
        if (!this.state.loading) {
            this.setState({
                loading: true
            });
            console.log('takePicture ', this.camera.takePictureAsync)
            const options = { quality: 0.5, base64: true };
            const data = await this.camera.takePictureAsync(options)
            console.log(data);
            this.setState({ path: data.uri, baseImage: sampleImage.image })
        }
        this.setState({
            loading: false
        });
    }

    callGoogleVIsionApi = async ({ baseImage }) => {
        console.log('googleVisionRes called')
        let googleVisionRes = await fetch(config.googleCloud.api + config.googleCloud.apiKey, {
            method: 'POST',
            body: JSON.stringify({
                "requests": [
                    {
                        "image": {
                            "content": baseImage
                        },
                        "features": [
                            {
                                "type": "LABEL_DETECTION"
                            },
                            {
                                "type": "TEXT_DETECTION"
                            },
                            {
                                "type": "WEB_DETECTION"
                            },
                            {
                                "type": "LOGO_DETECTION"
                            },
                        ]
                    }
                ]
            })
        });
        console.log(googleVisionRes)
        googleVisionRes = await googleVisionRes.json()
        console.log(googleVisionRes)
        this.processVisionResults(googleVisionRes.responses[0])
        return googleVisionRes;
    }
    processVisionResults(v) {
        console.log(v)
        // process best guess labels and query
        let bestGuess = '';
        if (v.webDetection && v.webDetection.bestGuessLabels && v.webDetection.bestGuessLabels[0]) {
            bestGuess = v.webDetection.bestGuessLabels[0].label

        }

        // web results
        let webLabels = [];
        let bestGuessScore = [];
        if (v.webDetection && v.webDetection.webEntities) {
            v.webDetection.webEntities.forEach(n => {
                webLabels.push(n.description)
                bestGuessScore.push(n.score)

            })
        }

        // logo results
        let logoLabels = [];
        if (v.logoAnnotations) {
            v.logoAnnotations.forEach(n => {
                logoLabels.push(n.description)
            })
        }

        // text annotations
        let fullText = '';
        if (v.fullTextAnnotation && v.fullTextAnnotation.text) {
            let stringified = JSON.stringify(v.fullTextAnnotation.text);
            fullText = stringified.replace(/\\n/g, ' ')

            fullText = fullText.replace(/[^a-zA-Z ]/g, "");
            let fullTextWords = fullText.split(' ');

            fullText = '';

            fullTextWords.forEach(word => {
                if (word.length > 2) {
                    fullText = fullText + ' ' + word
                }
            })

            fullText = fullText.trim();

        }

        // process label detections and query
        let labels = [];
        let labelScore=[];
        if (v.labelAnnotations) {
            v.labelAnnotations.forEach(n => {
                labels.push(n.description)
                labelScore.push(n.score)

            })
        }
        
        console.log(labels);
        console.log(labelScore);
        console.log(bestGuess);
        console.log(webLabels);
        console.log(logoLabels);
        console.log(fullText);
        this.responseData(labels, bestGuess, webLabels, logoLabels, fullText,bestGuessScore,labelScore)
    }


    responseData = async (labels, bestGuess, webLabels, logoLabels, fullText,bestGuessScore,labelScore) => {
        console.log("labels", labels, "bestGuess", bestGuess, "webLabels", webLabels, "logoLabels", logoLabels, "fullText", fullText)
        let data = {
            labels: labels,
            bestGuess: bestGuess,
            webLabels: webLabels,
            logoLabels: logoLabels,
            fullText: fullText,
            bestGuessScore:bestGuessScore,
            labelScore:labelScore
        }
        console.log('data', data)
        let resultingData = await fetch("https://us-central1-savorapp-72shad1d.cloudfunctions.net/findBestMatch", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
        console.log("data is", data)
        console.log("visionResponse is", resultingData)
        resultingData = await resultingData.json()
        return resultingData;
    }

    /*-----Refresh------*/

    retakePic = () => {
        console.log('cancel')
        // alert('cancel')
        console.log(this.state.path, "path State")
        this.setState({
            path: null
        })
    }
    /*-----Cancel------*/

    cancel = () => {
        this.props.navigation.navigate('WelcomeHome')
    }

    /*----Save----*/
    save = async () => {
        console.log('save')
        // alert('save')
        console.log(this.state.path, "path State")
        let data = this.state.path
        let baseImage = this.state.baseImage;
        console.log(data, "data")
        this.setState({
            loading: true,
        })
        const googleVisionRes = await this.callGoogleVIsionApi({ baseImage: baseImage })
        console.log('googleVisionRes', googleVisionRes)
        this.props.navigation.navigate('ItemView', { data: data })
        this.setState({
            path: null,
            loading: false,
        })


    }
    renderButton = () => {
        if (this.state.loading) {
            return <SpinnerComponent size={'small'} />
        }
        else {
            console.log('loading failed to execute', this.state.loading)
        }
    }

    renderCamera() {
        console.log('Starting app');
        console.log('config', config);

        return (
            <Camera
                ref={(cam) => {
                    this.camera = cam;
                }}
                style={styles.cameraScreen}
                flashMode={Camera.Constants.FlashMode.off}
                permissionDialogTitle={'Permission to use camera'}
                permissionDialogMessage={'We need your permission to use your camera phone'}
                onGoogleVisionBarcodesDetected={({ barcodes }) => {
                    console.log(barcodes, "barcodes")
                }}>
                <View><Text>{this.state.loading}</Text></View>
                <View style={styles.cameraDiv}>
                    <Text style={styles.cancelText} onPress={this.cancel.bind(this)}>Cancel</Text>
                    <View style={styles.takePicture}>
                        <Icon type="FontAwesome" name='camera' onPress={this.takePicture.bind(this)} style={styles.takeIcon} />
                    </View>
                    <Icon type="FontAwesome" name='bolt' onPress={this.save.bind(this)} style={styles.flash} />
                </View>
            </Camera>
        );
    }

    renderImage() {
        return (
            <View style={styles.previewContainer} >
                <View style={styles.frameContainer}>
                    <View style={styles.imageFrameViewOutest}>
                    </View>
                    <View style={styles.imageFrameViewOuter}>
                    </View>
                    <View style={styles.imageFrameViewInner}>
                        <Image
                            source={{ uri: this.state.path }}
                            style={styles.imageStyle}
                        />
                    </View>
                </View>
                <View style={styles.buttonDiv}>
                    <Icon type="FontAwesome" name='arrow-left' onPress={this.cancel.bind(this)} style={styles.cancel} />
                    <View>
                        <Icon type="FontAwesome" name='refresh' onPress={this.retakePic.bind(this)} style={styles.retakePic} />
                        <Text style={styles.retakeText}>Retake</Text>
                    </View>
                    {this.state.loading ? <SpinnerComponent size={'small'} /> :
                        <Icon type="FontAwesome" name='check' onPress={this.save.bind(this)} style={styles.Save} />}
                </View>
            </View>
        );
    }


    render() {
        console.log('Starting app');
        console.log('config is:', config)
        return (
            <View style={styles.container}>
                {this.state.path ? this.renderImage() : this.renderCamera()}
            </View>
        );
    }
}


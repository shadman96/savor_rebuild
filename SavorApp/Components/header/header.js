import React, { Component } from 'react'
import { View, StyleSheet } from 'react-native'
import { Container, Header, Left, Body, Right, Button, Icon, Title, Content, H3 } from 'native-base';

const HeaderComponent = (props) => {
  const {onPress} = props
  return (
    <View>
      <Header style={{ backgroundColor: '#00405D' }}>
        <Left>
          <Button transparent>
            <Icon name={props.md} android={props.ios} onPress={onPress} />
          </Button>
        </Left>
        <Body style={styles.HeaderTitle}>
          <Title>{props.title}</Title>
        </Body>
        <Right>
          <Button transparent>
            <Icon name='more' />
          </Button>
        </Right>
      </Header>
    </View>

  )
}
const styles = StyleSheet.create({
  HeaderTitle: {
    alignItems: 'center'

  }
})

export default HeaderComponent;


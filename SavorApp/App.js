/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import firebase from 'react-native-firebase'
import { StackNavigator } from 'react-navigation'
import AppDrawerNavigator from '../SavorApp/Pages/DrawerNavigator/DrawerNavigator';
import HomeContainer from '../SavorApp/Pages/home/homeContainer';
import SignUpContainer from '../SavorApp/Pages/signUp/signUpcontainer';
import LoginContainer from '../SavorApp/Pages/login/loginContainer';
import ForgotPasswordContainer from '../SavorApp/Pages/ForgotPassword/ForgotPasswordContainer';
import CameraComponent from '../SavorApp/Components/camera/camera'
import GreetModal from './Components/greetModal/greetModal';

export default class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loggedIn: false
    };
  }

  componentWillMount() {
    firebase.initializeApp({
      apiKey: "AIzaSyCFBRcX2gYB6ZpgWjLJEbW1VJ7kHlhXMOE",
      authDomain: "savorapp-72shad1d.firebaseapp.com",
      databaseURL: "https://savorapp-72shad1d.firebaseio.com",
      projectId: "savorapp-72shad1d",
      storageBucket: "savorapp-72shad1d.appspot.com",
      messagingSenderId: "1072496229079"
    });
    firebase.app();
  }

  render() {
    return (
      <MyAppStackNavigator />
    );
  }
}

const MyAppStackNavigator = StackNavigator({
  HomeView: {
    screen: HomeContainer,
    navigationOptions: {
      header: null
    }
  },

  LoginView: {
    screen: LoginContainer,
    navigationOptions: {
      header: null
    }
  },

  ForgotPasswordContainer: {
    screen: ForgotPasswordContainer
  },

  SignUpView: {
    screen: SignUpContainer,
    navigationOptions: {
      header: null
    }
  },

  CameraComponent: {
    screen: CameraComponent,
    navigationOptions: {
      header: null
    }
  },

  AppDrawerNavigator: {
    screen: AppDrawerNavigator,
    navigationOptions: {
      header: null
    }
  },

  GreetModal: {
    screen: GreetModal
  }
},
  {
    //   navigationOptions: {
    //     gesturesEnabled: false
    //   }
  });





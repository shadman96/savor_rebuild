import React, { Component } from "react";
import firebase from 'react-native-firebase';
import SignUpView from "./signUpview";
import { Toast, Root } from 'native-base'
import ButtonComponent from "../../Components/button/buttonComponent";
import SpinnerComponent from "../../Components/spinner/spinner";

class SignUpContainer extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userReg: [],
            email: '',
            password: '',
            age: '',
            username: '',
            error: '',
            loading: false,
            flag: false
        };
        this.ref = firebase.firestore().collection('users');
    }

    componentDidMount() {
        this.unsubscribe = this.ref.onSnapshot((userSnapshot) => {
            const userReg = [];
            userSnapshot.forEach((doc) => {
                userReg.push({
                    newEmail: doc.data().newEmail,
                    newPassword: doc.data().newPassword,
                    newAge: doc.data().newAge,
                    newUsername: doc.data().newUsername
                });
            });
            this.setState({
                loading: false
            });
        });
    }

    Validation = () => {
        let emailreg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        let passreg = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;
        if (!this.state.email) {
            if (emailreg.test(this.state.email) === false) {
                Toast.show({
                    text: 'Invalid Email Details',
                    duration: 300,
                    position: 'top'
                });
                return false
            }
        }
        else if (!this.state.password || !this.state.password.trim().length) {
            Toast.show({
                text: 'Password can not be empty ',
                duration: 300,
                position: 'top'
            });
            return false
        }
        return true
    }

    inputChangeText = (value, type) => {
        // alert(type)
        this.setState({
            [type]: value
        });
    }
    static navigationOptions = {
        headerTintColor: '#fff',
        headerTransparent: true,
        headerStyle: { borderBottomWidth: 0, }
    }

    Submit = () => {
        if (this.Validation()) {
            Toast.show({
                text: "Signup SuccessFully",
                buttonText: "Okay",
                duration: 3000,
                position: "top"
            })
            data = { email: this.state.email, password: this.state.password }
            this.setState({ value: null });
            console.log(data)
            this.setState({ error: '', loading: true });
            firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.password, this.state.age, this.state.username)
                .then(this.onSignupSuccess.bind(this))
                .catch((err) => {
                    console.log(err.message)
                    Toast.show({
                        text: `${err.message}`,
                        duration: 3000,
                        position: "top"
                    })
                        .catch(this.onSignupFail.bind(this))
                    this.setState({ error: '', loading: false });
                });
        }
    };

    onSignupFail = () => {
        this.setState({
            loading: false,
            error: 'Authentication Failed.',
        });
    }

    renderButton = () => {
        if (this.state.loading) {
            return <SpinnerComponent size={'small'} />
        }
        else {
            return <ButtonComponent block={'block'} link={'Signup'} onPress={() => this.Submit()} />
        }
    }

    onSignupSuccess = () => {
        console.log("this.state", this.state)
        this.ref.add({
            newEmail: this.state.email,
            newPassword: this.state.password,
            newAge: this.state.age,
            newUsername: this.state.username
        }),
            Toast.show({
                text: `You have Successfully Signup`,
                duration: 3000,
                position: "top"
            }), () => {
                this.props.navigation.navigate('HomeView')
            }
    }

    LogIn = () => {
        this.props.navigation.navigate('LoginView')
    };

    goBack = () => {
        this.props.navigation.goBack()
    }

    render() {
        return (
            <Root>
                <SignUpView
                    Submit={this.Submit}
                    LogIn={this.LogIn}
                    inputChangeText={this.inputChangeText}
                    onSignupSuccess={this.onSignupSuccess}
                    goBack={this.goBack}
                    error={this.state.error}
                    renderButton={this.renderButton} />
            </Root>
        )
    }
}
export default SignUpContainer;

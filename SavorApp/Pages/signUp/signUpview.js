import React, { Component } from 'react';
import { View, Text, Dimensions } from 'react-native';
import styles from './signUpStyle';
import FormComponent from '../../Components/forms/form';
import ButtonLinkComponent from '../../Components/button/buttonLink';
import { Root, Container, Header, Content, Left, Button, Icon } from 'native-base';


const SignUpView = (props) => {
    return (
        <Container style={styles.container}>
            <Header style={styles.header} >
                <Left>
                    <Button transparent onPress={props.goBack}>
                        <Icon style={styles.headerIcon} name='arrow-back' />
                    </Button>
                </Left>
            </Header>
            <Content>
                <View style={styles.content}>
                    <View style={styles.Formcontainer}>
                        <Text style={styles.LogText}>Signup</Text>
                        <View>
                            <FormComponent User={'CHOOSE A USERNAME'} Age={'YOUR AGE'} Email={'EMAIL ADDRESS'} password={'PASSWORD'} secureTextEntry pass={props.password} email={props.email} age={props.age} username={props.username} inputChangeText={(text, type) => props.inputChangeText(text, type)} />
                        </View>
                        <View style={styles.buttonDiv}>
                            {props.renderButton()}
                            <ButtonLinkComponent link={'Already a member?'} onPress={() => props.LogIn()} />
                        </View>
                    </View>
                </View>
            </Content>
        </Container>
    )
}
export default SignUpView;
import React, { Component } from "react";
import { DrawerNavigator } from 'react-navigation';
import FooterComponent from "../../Components/footer/footerButton";
import ForgotPassword from "../ForgotPassword/ForgotPasswordView";
import WelcomeHomeContainer from "../welcome/welcomeContainer";
import ItemContainer from "../itemDetail/itemContainer";
const AppDrawerNavigator = new DrawerNavigator({
    WelcomeHome: {
        screen: WelcomeHomeContainer
    },
    ChangePassword: {
        screen: ForgotPassword
    },
    ItemView: {
        screen: ItemContainer
    }
},
    {
        initialRouteName: 'WelcomeHome',
        contentComponent: FooterComponent,
        drawerPosition: 'Left',
        drawerOpenRoute: 'DrawerOpen',
        drawerCloseRoute: 'DrawerClose',
        drawerToggleRoute: 'DrawerToggle'
    })
export default AppDrawerNavigator;
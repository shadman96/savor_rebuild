import React, { Component } from 'react'
import { View } from 'react-native'
import ForgotPassword from './ForgotPasswordView';
import { Toast ,Root} from 'native-base'

class ForgotPasswordContainer extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: ''
        }
    }
    Validation = () => {
        let emailreg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (!this.state.email) {
            if (emailreg.test(this.state.email) === false) {
                Toast.show({
                    text: "Enter Valid Email Address",
                    duration: 3000,
                    position: "top"
                });
                return false
            }
        }
        return true
    }
    inputChangeText = (value, type) => {
        // alert(type)
        this.setState({
            [type]: value
        });
    }
    Submit = () => {
        if (this.Validation()) {
            Toast.show({
                text: "Submitted SuccessFully",
                buttonText: "Okay",
                duration: 3000,
                position: "top"
            })
            data = { email: this.state.email }
            this.setState({ value: null });
            console.log(data)
            AsyncStorage.setItem('user', JSON.stringify(data))
                .catch((err) => {
                    console.log('err', err)
                })
            this.props.longin(data)
        }
        // this.props.navigation.navigate('AppDrawerNavigator')
    }
    static navigationOptions = {
        headerTintColor: '#fff',
        headerTransparent: true,
        headerStyle: { borderBottomWidth: 0, }
    }
    render() {
        return (
            <Root>
            <ForgotPassword 
            Submit={this.Submit} inputChangeText={this.inputChangeText} />
            </Root>
        )
    }
}

export default ForgotPasswordContainer
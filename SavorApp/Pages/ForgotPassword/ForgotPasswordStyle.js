import { StyleSheet, Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window')

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#0F587A',
    },
    Formcontainer: {
        paddingRight: 30,
        paddingLeft: 30,
        alignSelf: 'center',
        width: 0.88* width
    },
    LogText: {
        color: 'rgb(255,255,238)',
        fontSize: 40,
        fontWeight: '200',
        paddingBottom: 20
    },
    DescText:{
        fontSize: 14,
        fontWeight: 'normal',
        color:'white'
    },
    LogFooter: {
        bottom: 0,
        position: 'absolute',
        backgroundColor: '#1476A5',
        right: 0,
        left: 0
    },
    buttonDiv: {
        display: 'flex',
        marginTop: 30,
        justifyContent: 'flex-start',
        width: 70
    }
})
export default styles;

import React, { Component } from 'react'
import { View, Text } from 'react-native'
import ButtonComponent from '../../Components/button/buttonComponent';
import FormComponent from '../../Components/forms/form';
import styles from './ForgotPasswordStyle';
import { Root } from 'native-base';


const ForgotPassword = (props) => {
    return (
        <Root>
            <View style={styles.container}>
                <View style={styles.Formcontainer}>
                    <Text style={styles.LogText}>Forgot Password</Text>
                    <View>
                        <Text style={styles.DescText}>{"Enter your email address, we'll send you the instructions on how to change your password"}</Text>
                    </View>
                    <View>
                        <FormComponent Email={'EMAIL ADDRESS'} email={props.email} inputChangeText={(text, type) => props.inputChangeText(text, type)} />
                    </View>
                    <View style={styles.buttonDiv}>
                        <ButtonComponent style={styles.btn} link={'SEND'} block={'block'} onPress={() => props.Submit()} />
                    </View>
                </View>
            </View>
        </Root>
    )
}


export default ForgotPassword
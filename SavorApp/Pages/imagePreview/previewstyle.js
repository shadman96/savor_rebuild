import {
    View,
    StyleSheet,
    Dimensions,
    TouchableHighlight,
    Image,
    Text,
    ImageBackground
} from 'react-native';import { AppStyles, AppSizes } from '@theme/';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
    },
    cameraScreen: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width
    },
    capture: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 50,
        padding: 10,
        margin: 50,
        height: 70,
        width: 70,
        borderColor: 'rgba(0, 0, 0, 0.3)',
        borderWidth: 15
    },
    loadingMsg: {
        position: 'absolute',
        top: '50%',
        left: '50%'
    },
    loadingText: {
        fontSize: 18,
        padding: 5,
        borderRadius: 20,
        backgroundColor: 'white',
        margin: 30
    },
    spinner: {
        marginBottom: 50
    },
    cancel: {
        backgroundColor: '#aab1b5',
        borderRadius: 95,
        color: '#FFF',
        fontWeight: '600',
        fontSize: 40,
        padding: 18,
    },

    Save: {
        backgroundColor: '#00a99d',
        padding: 18,
        borderRadius: 90,
        color: '#FFF',
        fontWeight: '600',
        fontSize: 40,
    },
    retakePic: {
        backgroundColor: '#3A6579',
        borderRadius: 90,
        color: '#FFF',
        fontWeight: '600',
        fontSize: 18,
        padding: 18,
    },
    retakeText: {
        textAlign: 'center',
        color: 'white'
    },
    buttonDiv: {
        width: '100%',
        display: 'flex',
        bottom: 20,
        flexDirection: 'row',
        paddingLeft: 16,
        alignItems: 'flex-end',
        paddingHorizontal: 16,
        justifyContent: 'space-between'
    },
    cameraDiv: {
        width: '100%',
        display: 'flex',
        bottom: 30,
        flexDirection: 'row',
        paddingLeft: 16,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 16,
        justifyContent: 'space-between'
    },
    takePicture: {
        backgroundColor: 'white',
        borderRadius: 90,
        fontWeight: '600',
        fontSize: 25,
        padding: 15,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: 'rgba(0, 0, 0, 0.3)',
        borderWidth: 15,
    },
    takeIcon: {
        color: '#3A6579',
    },
    flash: {
        backgroundColor: '#3A6273',
        padding: 18,
        borderRadius: 80,
        color: '#FFF',
        fontWeight: '600',
        fontSize: 18,
    },
    cancelText: {
        color: '#FFF',
        fontWeight: '600',
        fontSize: 18,
    },

    previewContainer: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: '#016290',
    },
    preview: {
        height: 300,
        width: 280,
    },

    frameContainer: {
        position: 'relative',
        height: AppSizes.screen.height * 0.7,
        // paddingHorizontal : 20,
        // paddingHorizontal : 20
    },
    imageFrameViewOutest: {
        top: AppSizes.screen.height * 0.13,
        width: AppSizes.screen.width * 0.7,
        height: AppSizes.screen.width * 0.8,
        marginHorizontal: AppSizes.screen.width * 0.15,
        marginVertical: 10,
        backgroundColor: '#ffffffd6',
        transform: [{ rotate: '-5deg' }],
        position: 'absolute'
    },
    imageFrameViewOuter: {
        top: AppSizes.screen.height * 0.13,
        width: AppSizes.screen.width * 0.7,
        height: AppSizes.screen.width * 0.8,
        marginHorizontal: AppSizes.screen.width * 0.15,
        marginVertical: 10,
        backgroundColor: '#ffffff80',
        transform: [{ rotate: '-10deg' }],
        position: 'absolute'
    },
    imageFrameViewInner: {
        top: AppSizes.screen.height * 0.13,
        width: AppSizes.screen.width * 0.7,
        height: AppSizes.screen.width * 0.8,
        marginHorizontal: AppSizes.screen.width * 0.15,
        marginVertical: 10,
        backgroundColor: '#fff',
        position: 'absolute',
        paddingHorizontal: 15,
        paddingVertical: 15
    },
    imageStyle: {
        width: '100%',
        height: '100%'
    },
    backgroundImage: {
        // flex: 1,
        resizeMode: 'cover',
        height: 200,
        width: 200
    },

})
export default styles;

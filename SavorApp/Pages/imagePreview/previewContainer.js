import React, { Component } from 'react'
import { View } from 'react-native'
import firebase from 'react-native-firebase'
import SpinnerComponent from '../../Components/spinner/spinner';

class PreviewContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            path: null,
            loading: false,
        };
    }

    takePicture = async () => {
        if (!this.state.loading) {
            this.setState({
                loading: true
            });
            console.log('takePicture ', this.camera.takePictureAsync)
            const options = { quality: 0.5, base64: true };
            const data = await this.camera.takePictureAsync(options)
            console.log(data);
            this.setState({ path: data.uri })
        }
        this.setState({
            loading: false
        });
    }

    retakePic = () => {
        console.log('cancel')
        // alert('cancel')
        console.log(this.state.path, "path State")
        this.setState({
            path: null
        })
    }

    cancel = () => {
        this.props.navigation.navigate('WelcomeHome')
    }

    /*----Save----*/
    save = () => {
        console.log('save')
        // alert('save')
        console.log(this.state.path, "path State")
        let data = this.state.path
        console.log(data, "data")
        this.setState({
            path: null,
            loading: true
        })
        const googleVisionRes = await this.callGoogleVIsionApi(data.base64);
        console.log('googleVisionRes', googleVisionRes)
        this.props.navigation.navigate('ItemView', { data: data })
        this.setState({
            loading: false
        })
    }
    renderButton = () => {
        if (this.state.loading) {
            return <SpinnerComponent size={'small'} onPress={()=> this.save()}/>
        }
        else {
            console.log('loading failed to execute',this.state.loading)
        }
    }
    callGoogleVIsionApi = async (base64) => {
        let googleVisionRes = await fetch(config.googleCloud.api + config.googleCloud.apiKey, {
            method: 'POST',
            body: JSON.stringify({
                "requests": [
                    {
                        "image": {
                            "content": base64
                        },
                        "features": [
                            {
                                "type": "LABEL_DETECTION"
                            },
                            {
                                "type": "TEXT_DETECTION"
                            },
                            {
                                "type": "WEB_DETECTION"
                            },
                            {
                                "type": "LOGO_DETECTION"
                            },
                        ]
                    }
                ]
            })
        })
        googleVisionRes = await googleVisionRes.json()
            .then(googleVisionRes => {
                console.log(googleVisionRes)
                this.processVisionResults(googleVisionRes.responses[0])
            })
        return googleVisionRes;
    }
    processVisionResults(v) {

        // process best guess labels and query
        let bestGuess = '';
        if (v.webDetection && v.webDetection.bestGuessLabels && v.webDetection.bestGuessLabels[0]) {
            bestGuess = v.webDetection.bestGuessLabels[0].label
        }

        let webLabels = [];
        if (v.webDetection && v.webDetection.webEntities) {
            v.webDetection.webEntities.forEach(n => {
                webLabels.push(n.description)
            })
        }

        // process label detections and query
        let labels = [];
        v.labelAnnotations.forEach(n => {
            labels.push(n.description)
        })

        console.log(labels);
        console.log(bestGuess);
        console.log(webLabels);
    }



    render() {
        return (
            <CameraComponent
                save={this.save}
                cancel={this.cancel}
                takePicture={this.takePicture}
                retakePic={this.retakePic}
                callGoogleVIsionApi={this.callGoogleVIsionApi}
                path={this.state.path}
                loading={this.state.loading} />
        )
    }
}

export default PreviewContainer;
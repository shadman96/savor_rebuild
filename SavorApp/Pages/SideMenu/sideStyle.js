import { StyleSheet } from "react-native";
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'green',
    },
    header:{
        height:'30px',
    },
    activityBtn:{
        paddingTop:'2%'
    },
    btnview:{
        height:'100%',
        backgroundColor:'#E4E4E4',
    },
    btn:{
        padding:'10%',
    },
    signBtn:{
        paddingTop:'20%',
    },
    textColor:{
      
        color:'#0379FF',
        fontSize:16,
        paddingBottom:10
    },
    footerText:{
        justifyContent: 'center',
        alignItems: 'center',
        display:'flex'
    }
});
export default styles;
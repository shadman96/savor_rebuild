import React, { Component } from 'react'
import { View, Text, Image, ScrollView, Slider, TextInput } from 'react-native'
import { Container, Header, Left, Picker, Body, Right, Button, Icon, Title, Content, H2, List, ListItem, Form, Item, Input, Label } from 'native-base';
import HeaderComponent from '../../Components/header/header';
import styles from './itemStyle';
import ButtonComponent from '../../Components/button/buttonComponent'
import PickerComponent from '../../Components/piceker/picker';
import FlavorProfile from '../../Components/flavorProfile/flavor';
import RatingComponent from '../../Components/starRating/rating';
import GreetModal from '../../Components/greetModal/greetModal';


class ItemView extends Component {
  constructor(props) {
    state = {
      value: 0.2
    };
    super(props);
    this.state = {
      PickerCategory: 'key1',
      PickerType: 'key3'
    };
  }
  onValueChange({ value: itemValue }) {
    console.log("itemValue", itemValue)
    this.setState({
      PickerCategory: itemValue,
      PickerType: itemValue
    });
  }
  render() {
    console.log(this.props, "this.props")
    return (
      <Container style={styles.Container}>
        <HeaderComponent md={"md-menu"} ios={"ios-menu"} title={"Item Details"} />
        <Content style={{ paddingHorizontal: 16 }}>
          <View style={styles.CameraRight}>
            <View style={styles.camIconRight}>
              <Icon name="camera" android="md-camera" style={styles.camIconRightInner} />
            </View>
          </View>
          <View style={styles.cameraIcon}>
            <View style={styles.CameraCenter}>
              <View style={styles.camIconCenter}>
                <Image source={{ uri: this.props.img }} style={styles.camIconCentertInner} />
              </View>
            </View>
          </View>
          <View style={styles.ContentSize}>
            <View style={styles.FromTop}>
              <View style={styles.TextLeft}>
                <View style={{ alignItems: 'flex-end' }}>
                  <Text style={styles.TextAv}>1498</Text>
                  <Text style={styles.TextPrice}>Ratings</Text>
                </View>
                <Image source={require('../../images/savor_icon.png')} style={{ width: 100, height: 90 }} />
              </View>
              <View style={styles.TextRight}>
                <View style={{ alignItems: 'flex-end' }}>
                  <Text style={styles.TextPrice}>{"$$$$"}</Text>
                  <Text style={styles.TextTotal}>{"$114.9"}</Text>
                  <Text style={styles.TextAv}>{"Average Price"}</Text>
                </View>
              </View>
              <View style={styles.ItemDescription}>
                <Text style={styles.TextTotal}>Hibiki 12</Text>
                <H2 style={styles.TextPos}>Suntoty Whisky</H2>
                <Text style={{ textAlign: 'center' }}>Description and testing notes and whatever other information  we have goes here</Text>
              </View>
              <View style={{ alignItems: 'center', paddingVertical: 16 }}>
                <Text style={{ textAlign: 'center' }}>Tap to rate,slide for half star</Text>
                <RatingComponent
                  starCount={this.props.starCount}
                  textChange={(a, b) => this.props.textChange(a, b)}
                />
              </View>
              <View style={styles.Toatalwidth}>
                <ListItem itemDivider style={{ left: 16 }}>
                  <Label>GENERAL</Label>
                </ListItem>
              </View>
              <PickerComponent
                textChange={(a, b, c) => this.props.textChange(a, b, c)}
                PickerType={this.props.PickerType}
                PickerCategory={this.props.PickerCategory}
                Distillery={this.props.Distillery}
                Vintage={this.props.Vintage}
                Age={this.props.Age}
              />
              <View style={styles.FlavorPicker}>
                <ListItem itemDivider>
                  <Label>{"FLAVOR PROFILE"}</Label>
                </ListItem>
                <FlavorProfile flavorProfile={this.props.flavorProfile}
                  flavor={this.props.flavor}
                  textChange={(a, b) => this.props.textChange(a, b)}
                  flavorValueChange={this.props.flavorValueChange}
                />
              </View>
              <View style={styles.PickerButton}>
                <ButtonComponent link={'Submit'} block={'block'} onPress={() => this.props.submitForm()} />
              </View>
            </View>
          </View>
          <GreetModal isModalVisible={this.props.isModalVisible} toggleModal={() => this.props.toggleModal()} />
        </Content>
      </Container>
    )
  }
}

export default ItemView;
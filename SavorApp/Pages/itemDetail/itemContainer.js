import React, { Component } from 'react'
import { View } from 'react-native'
import ItemView from './itemView';
import firebase from 'react-native-firebase';

const flavorProfile = [
    'Smokey',
    'Peaty',
    'Spicy',
    'Herbal',
    'Oily',
    'Full',
    'Rich',
    'Sweet',
    'Briny',
    'Salty',
    'Vanilla',
    'Tart',
    'Fruity',
    'Floral'
];

class ItemContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            PickerCategory: 'key1',
            PickerType: 'key3',
            Distillery: '',
            Vintage: '',
            Age: 0,
            flavor: new Array(flavorProfile.length).fill(0),
            starCount: 2.5,
            loading:false,
            modal:true
        };
        this.state = {
            flavor: new Array(flavorProfile.length).fill(0)
        };
        this.ref = firebase.firestore().collection('recipe');
    }

    submitForm = () => {
        const { params } = this.props.navigation.state
        /*-----FireStore---- */
        this.ref.add({
            imageUrl: params.data,
            age: this.state.Age,
            pickerCategory: this.state.PickerCategory,
            PickerType: this.state.PickerType,
            distillery: this.state.Distillery,
            vintage: this.state.Vintage,
            starCount: this.state.Vintage,
            flavor: this.state.flavor
        })
            .then((response) => {
                console.log(`added data=`,response );
                this.setState({
                    path: '',
                    loading: true,
                    modal:true
                })
            })
            .catch((err) => {
                console.log(err)
                Toast.show({
                    text: `${err.message}`,
                    duration: 3000,
                    position: "top"
                })

            });
        /*-----End of FireStore---- */
    }

    flavorValueChange = (update, index) => {
        this.state.flavor[index] = update;
        this.setState({
            flavor: this.state.flavor
        })
    }

    textChange = ({ Name: itemValue, id: b, class: c }) => {
        console.log("itemValue", itemValue, b, "b", c)
        if (c === 'category') {
            this.setState({
                PickerCategory: itemValue,
            });
        }
        else if (c === 'type') {
            this.setState({
                PickerType: itemValue
            });
        }
        else if (c === 'Distillery') {
            this.setState({
                Distillery: itemValue
            });
        }
        else if (c === 'Vintage') {
            this.setState({
                Vintage: itemValue
            });
        }
        else if (c === 'Age') {
            this.setState({
                Age: itemValue
            });
        }
        else if (c === 'Star') {
            this.setState({
                starCount: itemValue
            });
        }
        console.log(this.state)

    }

    toggleModal(){
        this.setState({
            modal : false
        })
    }

    render() {
        const { params } = this.props.navigation.state
        console.log(params, "params")
        console.log(this.props, this.state.flavor, "this.propscontainer")
        return (
            <ItemView
                img={params.data}
                submitForm={this.submitForm}
                PickerCategory={this.state.PickerCategory}
                PickerType={this.state.PickerType}
                Distillery={this.state.Distillery}
                Vintage={this.state.Vintage}
                Age={this.state.Age}
                flavorProfile={flavorProfile}
                flavor={this.state.flavor}
                textChange={this.textChange}
                flavorValueChange={this.flavorValueChange}
                starCount={this.state.starCount}
                isModalVisible={this.state.modal}
                toggleModal={()=>this.toggleModal()} />
        )
    }
}
export default ItemContainer;
import { StyleSheet, Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window')
import { AppSizes } from '@theme';


const styles = StyleSheet.create({
    Container: {
        // flex: 1,
        backgroundColor: '#0F587A',
    },
    SideImages: {
        // display: 'flex',
        flexDirection: 'row',
        backgroundColor: 'red',
        // justifyContent: 'space-around',
        width: width,
    },
    CameraArea: {
        width: AppSizes.screen.width - 40,
        height: AppSizes.screen.width,
        backgroundColor: 'white',
        borderRadius: 15,
        marginHorizontal: 40,
        marginBottom: 16,
        marginTop: 60,
        overflow: 'visible',
        zIndex: 99999,
        alignItems: 'center',
        justifyContent: 'flex-start'
    },
    card: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    camIconViewOuter: {
        backgroundColor: '#016290',
        borderColor: 'white',
        borderWidth: 8,
        borderRadius: 100,
        height: 100,
        width: 100,
        top: 20,
        position: 'absolute',
        zIndex: 99999,
        justifyContent: 'center',
        alignItems: 'center',
    },
    camIconViewInner: {
        backgroundColor: '#016290',
        borderRadius: 80,
        fontSize: 50,
        color: 'white'
    },
    Formcontainer: {
        // paddingLeft: 16,
        // paddingRight: 16,
        top: 25,
        justifyContent: 'center',
        alignItems: 'center',
    },
    DescText: {
        color: '#DBDBDB',
        paddingBottom: 15,
        fontWeight: 'bold',
        fontSize: 16,
        fontFamily: 'HelveticaNeue',
    },
    TextLeft: {
        position: 'absolute',
        left: 0,
        top: -25,
        zIndex: 111
    },
    TextRight: {
        position: 'absolute',
        right: 16,
        top: -25,
    },
    ButtonPos: {
        display: 'flex',
        flexDirection: 'row',
        marginTop: 30,
        justifyContent: 'space-between'
    },
    TextTotal: {
        fontWeight: 'bold',
        textAlign: 'justify',
        fontSize: 35,
        color: '#0F587A'
    },
    TextPrice: {
        color: 'red',
    },
    TextAv: {
        fontSize: 10
    },
    BottonPos: {
        top: 45,
    },
    ContentSize: {
        marginVertical: 80,
        backgroundColor: 'white',
        borderRadius: 10,
        paddingVertical:50,
    },
    GeneralPicker:{
        width:300
     },
     PickerButton:{
        width:300,
        marginTop:16
     },
     FlavorPicker:{
         marginTop:10
     },
     Toatalwidth:{
     width:width
     },
    FromTop:{
        alignItems: 'center',
    },
    ItemDescription:{
        alignItems: 'center',
        marginTop:45
    },
    cameraIcon:{
        alignItems: 'center',
        justifyContent: 'center'
    },
    CameraCenter: {
        position: 'absolute',
        top: 25,
        backgroundColor: '#016290',
        borderColor: 'white',
        borderWidth: 8,
        borderRadius: 100,
        height: 110,
        width: 110,
        zIndex: 99999,
        // right: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    camIconCenter: {
        fontSize: 30,
    },
    camIconCentertInner: {
        color: 'white',
        height:90,
        width:90,
        borderRadius: 100,
    },
    CameraRight: {
        position: 'absolute',
        backgroundColor: 'white',
        borderColor: '#3A7C99',
        borderWidth: 8,
        top: 20,
        width: 50,
        height: 50,
        zIndex: 99999,
        borderRadius: 100,
        right: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    camIconRight: {
        // borderRadius: 80,
        fontSize: 20,
    },
    camIconRightInner: {
        color: '#0F587A',
    },
    camIconViewOuter: {
        backgroundColor: 'white',
        borderRadius: (AppSizes.screen.width - 40) / 3,
        height: (AppSizes.screen.width - 40) / 3,
        width: (AppSizes.screen.width - 40) / 3,
        top: - (AppSizes.screen.width - 40) / 6,
        position: 'absolute',
        zIndex: 2,
        justifyContent: 'center',
        alignItems: 'center',
    },
})
export default styles;

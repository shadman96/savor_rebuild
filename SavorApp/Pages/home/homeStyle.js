import { StyleSheet, Dimensions } from "react-native";
const { width, height } = Dimensions.get('window')
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#0F587A',
    },
    ImageCustom: {
        width: 0.4*width,
        height: 0.42*width
    },
    content:{
        alignSelf: 'center'
    },
    ImgPos: {
        width: width,
        justifyContent: 'center',
        alignItems: 'center',
    },
    titleDiv:{
        width: width,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 25
    },
    title:{
        width: 0.6*width,
        height: 0.3*width
    },
    buttonLogin:{
        width:0.6*width
    },
    activityBtn: {
        paddingTop: '2%'
    },
    btnview: {
        height: '100%',
        backgroundColor: '#E4E4E4',
    },
    btn: {
        padding: '10%',
    },
    buttonDiv:{
        width: width,
        justifyContent: 'center',
        alignItems: 'center'
    },
    signBtn: {
        paddingTop: '20%',
    },
    textColor: {
        color: '#0379FF',
        fontSize: 16,
        paddingBottom: 10
    },
    footerText: {
        justifyContent: 'center',
        alignItems: 'center',
        display: 'flex'
    },
    fromTop: {
        top: '40%'
    },
    SignUp: {
        paddingTop: '85%'
    }
});
export default styles;
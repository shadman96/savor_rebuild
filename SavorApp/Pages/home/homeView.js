import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import styles from './homeStyle';
import ButtonComponent from '../../Components/button/buttonComponent';
import ButtonLinkComponent from '../../Components/button/buttonLink';
// const { width, height } = Dimensions.get('window');

const HomeView = (props) => {

    return (
        <View style={styles.container}>
            <View style={styles.content}>
                <View style={styles.ImgPos}>
                    <Image source={require('../../images/savor_icon.png')} style={styles.ImageCustom} />
                </View>
                <View style={styles.titleDiv}>
                    <Image source={require('../../images/savor_title.png')} style={styles.title} />
                </View>
                <View style={styles.buttonDiv}>
                    <View style={styles.buttonLogin}>
                        <ButtonComponent link={'LOGIN'} onPress={() => props.LogIn()} />
                    </View>
                    <View>
                        <ButtonLinkComponent link={'Sign Up. It\'s FREE'} onPress={() => props.SignUp()} />
                    </View>
                </View>
            </View>
        </View>
    )
}
export default HomeView;
import React, { Component } from "react";
import HomeView from './homeView';
import firebase from 'react-native-firebase'


class HomeContainer extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            password: '',
            error: '',
            loading: false,
            loggedIn: false
        };
    }
    static navigationOptions = {
        header: null
    }
    LogIn = () => {
        this.props.navigation.navigate('LoginView')
    }
    SignUp = () => {
        this.props.navigation.navigate('SignUpView')
    }
    componentWillMount() {
        firebase.auth().onAuthStateChanged((users) => {
            console.log(users)
            if (users) {
                this.setState({ loggedIn: true })
            } else {
                this.setState({ loggedIn: false })
            }
            this.authRedirect();
        });
    }
    authRedirect = () => {
        if (this.state.loggedIn === true) {
            this.props.navigation.navigate('AppDrawerNavigator')
        }
        else {
            this.props.navigation.navigate('HomeView')
        }
    }
    render() {
        return (
            <HomeView LogIn={this.LogIn} SignUp={this.SignUp} />
        )
    }
}
export default HomeContainer;

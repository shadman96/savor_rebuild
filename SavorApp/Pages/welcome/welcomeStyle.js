import { StyleSheet, Dimensions } from 'react-native';
// const { width, height } = Dimensions.get('window')
import { AppStyles, AppSizes } from '@theme';


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#0F587A',
    },
    HeaderTitle: {
        alignItems: 'center',
        left: 0,
        right: 0
    },
    CameraArea: {
        width: AppSizes.screen.width - 80,
        height: AppSizes.screen.width - 126,
        backgroundColor: 'white',
        borderRadius: 10,
        marginHorizontal: 40,
        marginBottom: 16,
        marginTop: 60,
        overflow: 'visible',
        zIndex: 99999,
        alignItems: 'center',
        justifyContent: 'flex-start'
    },
    card: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    camIconViewOuter: {
        backgroundColor: '#016290',
        borderColor: 'white',
        borderWidth: 8,
        borderRadius: 100,
        height: 100,
        width: 100,
        bottom: 220,
        position: 'absolute',
        zIndex: 99999,
        justifyContent: 'center',
        alignItems: 'center',
    },
    camIconViewInner: {
        backgroundColor: '#016290',
        borderRadius: 80,
        fontSize: 50,
        color: 'white'
    },
    Formcontainer: {
        paddingLeft: 30,
        paddingRight: 30,
        top: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    DescText: {
        color: '#DBDBDB',
        paddingBottom: 15,
        fontWeight: 'bold',
        fontSize: 16,
        fontFamily: 'HelveticaNeue',
        textAlign:'center'
    },
    ImageCustom: {
        width: 310,
        height: 130,
        alignItems: 'center',
    },
    TextPos: {
        top: 40,
        fontWeight: 'bold',
        textAlign: 'justify'
    },
    BottonPos: {
        top: 45,
    },
    LogText:{
        textAlign:'center'
    }
})
export default styles;

import React, { Component } from "react";
import WelcomeHome from "./welcomeView";
class WelcomeHomeContainer extends Component {
    static navigationOptions = {
        // header: null,
        title: 'Welcome'
    }
    toggleDrawer = () => {
        this.props.navigation.toggleDrawer()
    }
    openCamera = () =>{
        this.props.navigation.navigate('CameraComponent')
    }
     render(){
         return(
            <WelcomeHome toggleDrawer={this.toggleDrawer} OpenDetails={this.OpenDetails} openCamera={this.openCamera} /> 
         )
     }
}
export default WelcomeHomeContainer;

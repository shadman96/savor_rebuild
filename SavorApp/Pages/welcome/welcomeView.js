import React, { component } from 'react';
import { styleSheet, View, Text, Image } from "react-native";
import { Container, Header, Left, Body, Right, Button, Icon, Title, Content, H3 } from 'native-base';
import styles from './welcomeStyle'
import ButtonComponent from '../../Components/button/buttonComponent';
import HeaderComponent from '../../Components/header/header';

const WelcomeHome = (props) => {

  return (
    <Container style={styles.container}>
      <HeaderComponent md={"md-menu"} ios={"ios-menu"} title={'Welcome'} onPress={() => props.toggleDrawer()} />
      <View style={styles.Formcontainer}>
        <View >
          <Image source={require('../../images/savor_icon.png')} style={styles.ImageCustom} />
        </View>
        <View style={styles.LogText}>
          <Text style={styles.DescText}>{"Welcome to Savor's Prototype App! Use the 'Take a Picture'button below to submit an item to our image recognition engine."} </Text>
        </View>
        <View style={styles.card}>
          <View style={styles.CameraArea}>
            <H3 style={styles.TextPos}>Take a Picture android</H3>
            <Text secondary style={styles.TextPos}>{"Take a Picture of an object,label"}</Text>
            <Text secondary style={styles.TextPos}>{"or barcode and we'll add it to our"}</Text>
            <Text secondary style={styles.TextPos}>{"database."}</Text>
            <View style={styles.BottonPos}>
              <ButtonComponent link={"Take a Picture"} block={'block'} onPress={() => props.openCamera()} />
            </View>
          </View >
          <View style={styles.camIconViewOuter}>
            <Icon name="camera" android="md-camera" style={styles.camIconViewInner} />
          </View>
        </View>
      </View>
    </Container>
  )
}
export default WelcomeHome;


import React, { Component } from 'react';
import { View, Text, Dimensions } from 'react-native';
import styles from './loginStyle';
import FormComponent from '../../Components/forms/form';
import ButtonLinkComponent from '../../Components/button/buttonLink';
import { Root, Container, Header, Content, Left, Button, Icon } from 'native-base';

const LoginView = (props) => {
    console.log(props)

    return (
        <Container style={styles.container}>
            <Header style={styles.header} >
                <Left>
                    <Button transparent onPress={props.goBack}>
                        <Icon style={styles.headerIcon} name='arrow-back' />
                    </Button>
                </Left>
            </Header>
            <Content>
                <View style={styles.content}>
                    <View style={styles.Formcontainer}>
                        <Text style={styles.LogText}>Login</Text>
                        <View>
                            <FormComponent Email={'EMAIL ADDRESS'} password={'PASSWORD'} secureTextEntry pass={props.password} email={props.email} inputChangeText={(text, type) => props.inputChangeText(text, type)} />
                        </View>
                        <Text style={styles.reeorTextStyle}>{props.error}</Text>
                        <View style={styles.buttonDiv}>
                            {props.renderButton()}
                            <ButtonLinkComponent link={"Forgot Password?"} onPress={() => props.ForgotPassword()} />
                        </View>
                    </View>
                    <View style={styles.LogFooter}>
                        <ButtonLinkComponent link={"Don't have an account? Signup"} onPress={() => props.SignUp()} />
                    </View>
                </View>
            </Content>
        </Container>
    )
}
export default LoginView;


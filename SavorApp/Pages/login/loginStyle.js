import { StyleSheet, Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window')

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#0F587A',
        height: height
    },
    content: {
        justifyContent: 'center',
        alignItems: 'center',
        height: height - 128
    },
    header: {
        shadowOffset: { height: 0, width: 0 },
        shadowOpacity: 0,
        backgroundColor: '#0F587A',
        elevation: 0,
        justifyContent: 'flex-start',
        borderBottomWidth: 0
    },
    headerIcon: {
        color: 'white'
    },
    Formcontainer: {
        paddingRight: 30,
        paddingLeft: 30,
        alignSelf: 'center',
        width: 0.88 * width
    },
    LogText: {
        color: 'rgb(255,255,238)',
        fontSize: 40,
        fontWeight: '200',
        paddingBottom: 20
    },
    LogFooter: {
        bottom: 0,
        position: 'absolute',
        backgroundColor: '#0F587A',
        right: 0,
        left: 0
    },
    buttonDiv: {
        display: 'flex',
        marginTop: 15,
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 0
    },
    reeorTextStyle: {
        color: 'red',
        fontSize: 20,
        alignItems: 'center'
    }
})
export default styles;

import React, { Component } from "react";
import firebase from 'react-native-firebase'
import LoginView from './LoginView';
import { Toast, Root } from 'native-base'
import SpinnerComponent from "../../Components/spinner/spinner";
import ButtonComponent from "../../Components/button/buttonComponent";

class LoginContainer extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            password: '',
            error: '',
            loading: false,
            loggedIn: false
        };
    }
    static navigationOptions = {
        headerTintColor: '#fff',
        headerTransparent: true,
        headerStyle: { borderBottomWidth: 0, }
    }

    Validation = () => {
        let emailreg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        let passreg = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;
        if (!this.state.email) {
            if (emailreg.test(this.state.email) === false) {
                Toast.show({
                    text: " Email Invalid Details",
                    duration: 3000,
                    position: "top"
                });
                return false
            }
        }
        else if (!this.state.password || !this.state.password.trim().length) {
            Toast.show({
                text: " Password Cannot be Empty",
                duration: 3000,
                position: "top"
            });
            return false
        }
        return true
    }

    inputChangeText = (value, type) => {
        this.setState({
            [type]: value
        });
    }

    LogIn = () => {
        if (this.Validation()) {
            // Toast.show({
            //     text: "Login SuccessFully",
            //     buttonText: "Okay",
            //     duration: 3000,
            //     position: "top"
            // })

            data = { email: this.state.email, password: this.state.password }
            this.setState({ value: null });
            console.log(data)
            this.setState({ error: '', loading: true });
            firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password)
                .then(this.onLoginSuccess.bind(this))
                .catch((err) => {
                    console.log(err)
                    Toast.show({
                        text: `${err.message}`,
                        duration: 3000,
                        position: "top"
                    })
                    this.setState({ error: '', loading: false });
                });
        }
    }

    renderButton = () => {
        if (this.state.loading) {
            return <SpinnerComponent size={'small'} />
        }
        else {
            return <ButtonComponent link={'LOGIN'} onPress={() => this.LogIn()} />
        }
    }

    onLoginSuccess = () => {
        this.setState({
            email: '',
            password: '',
            loading: false,
            error: '',
        }, () => {
            this.props.navigation.navigate('AppDrawerNavigator')
        });
    }

    onLoginFail = () => {
        this.setState({
            loading: false,
            error: 'Authentication Failed.',
        })
    }

    ForgotPassword = () => {
        this.props.navigation.navigate('ForgotPasswordContainer')
    }

    SignUp = () => {
        this.props.navigation.navigate('SignUpView')
    };

    goBack = () => {
        this.props.navigation.goBack()
    }

    render() {
        return (
            <Root>
                <LoginView
                    LogIn={this.LogIn}
                    ForgotPassword={this.ForgotPassword}
                    SignUp={this.SignUp}
                    inputChangeText={this.inputChangeText}
                    error={this.state.error}
                    renderButton={this.renderButton}
                    goBack={this.goBack} />
            </Root>
        )
    }
}
export default LoginContainer;